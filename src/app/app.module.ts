import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
//FireBase
import { environment } from './../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

//menu
import { RouterModule } from '@angular/router';

//form
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule,
 /*   RouterModule.forRoot([
      {path: '', component: UsersComponent}, // דף ראשי
      {path: 'users', component: UsersComponent},
      {path: 'user/:id', component: UserComponent},
      {path: 'update-form/:id', component: UpdateFormComponent},
      {path: 'products', component: ProductsComponent},
      {path: 'usersfire', component: UsersfireComponent},
      {path: '**', component: NotFoundComponent}//להשאיר בכל מקרה
    ])*/
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
